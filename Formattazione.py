class Formattazione:

    def scomponidati(self,testo,chardivisore):
        righe = testo.split(chardivisore)    #divido il database per righe
        righe.remove(righe[0])               #tolgo l'intestazione
        righe.remove(righe[righe.__len__() - 1])    #rimuovo l'ultima riga che è solo andata a capo
        return righe

    def raggruppacolonne(self,righe,indicicolonne,separatore):
        lista=[]                                        #lista degli attributi (colonne)
        for i in range(0,indicicolonne.__len__()):   #un attributo per ogni indice dato in ingresso
            lista.append([])

        for r in righe:                                 #per ogni riga
            riga = r.split(separatore)                   #divido la riga in valori seperati dal carattere separatore
            for i in range(0,indicicolonne.__len__()):
                lista[i].append(riga[indicicolonne[i]])    #ad ogni lista aggiungo il valore della colonna indicata in indicicolonne
        return lista

    def dividiperdiagnosi(self,colonne,diagnosi,separatore):
        lista=[]
        for n in range(0,colonne.__len__()):        #lista delle colonne di interessse
            lista.append([])
            for i in range(0,diagnosi.__len__()):   #ogni colonna avrà tante liste quante possibili diagnosi
                lista[n].append([])

        for n in range(0,colonne[0].__len__()):   #per ogni paziente (tutte le colonne hanno lo stesso numero di attributi)
            resultpaziente=colonne[colonne.__len__()-1][n].split(separatore)          #lista delle diagnosi per ogni paziente, più diagnosi possibili contemporaneamente
            resultpaziente.remove(resultpaziente[resultpaziente.__len__()-1])      #si elimina l'ultimo elemento delle diagnosi contemporanee che è un carattere vuoto
            for i in range(0,diagnosi.__len__()):                #per ogni possibile diagnosi
                if resultpaziente.__contains__(diagnosi[i]) and resultpaziente.__len__()==1:    #se la diagnosi è solo una
                    for c in range(0,lista.__len__()):          #per ogni attributo di interesse in lista aggiungiamo alla i-esima lista il paziente n
                        lista[c][i].append(colonne[c][n])                      #se result ha un solo elemento aggiungiamo gli attributi nella corrispondente lista
                elif resultpaziente.__contains__(diagnosi[i]) and i!=0:   #se result ha più elementi aggiungiamo gli attributi solo per diagnosi diverse da negative
                    for c in range(0,lista.__len__()):
                        lista[c][i].append(colonne[c][n])                 #negative viene aggiunto solo se è l'unico result, se i result sono più di uno la riga non viene usata
        return lista

    def convertiboolean(self,lista,caratteri0,caratteri1,sottoliste):
        listareturn=[]
        for n in range(0,lista.__len__()):        #lista di attributi
            listareturn.append([])
            if sottoliste:
                for i in range(0,lista[n].__len__()):      #lista di valori per ogni attributo
                    listareturn[n].append([])

        for c in range(0,lista.__len__()):        #ciclo per ogni attributo da convertire
            if sottoliste:                          #se la lista è fatta di sottoliste raggruppate per diagnosi
                for n in range(0,lista[c].__len__()):   #ciclo per ogni diagnosi di ogni attributo
                    for i in range(0,lista[c][n].__len__()):   #ciclo per ogni valore
                        if caratteri0.__contains__(lista[c][n][i]):      #Femmina o falso diventa 0
                            listareturn[c][n].append(0)
                        elif caratteri1.__contains__(lista[c][n][i]):   #Maschio o vero diventa 1
                            listareturn[c][n].append(1)
                        else:
                            listareturn[c][n].append(-1)      #tutto il resto è -1 cioè attributi mancanti o eventuali errori di battitura
            else:
                for i in range(0,lista[c].__len__()):   #ciclo per ogni valore
                        if caratteri0.__contains__(lista[c][i]):      #Femmina o falso diventa 0
                            listareturn[c].append(0)
                        elif caratteri1.__contains__(lista[c][i]):   #Maschio o vero diventa 1
                            listareturn[c].append(1)
                        else:
                            listareturn[c].append(-1)      #tutto il resto è -1 cioè attributi mancanti o eventuali errori di battitura

        return listareturn

    def convertiindice(self,lista,classifica,sottoliste):
        listareturn=[]
        if sottoliste:
            for n in range(0,lista.__len__()):        #lista di attributi
                listareturn.append([])

        #lista già divisa in sottoliste in base alla diagnosi oppure no
        if sottoliste:
            for n in range(0,classifica.__len__()):        #per ogni classificazione possibile
                    for i in range(0,lista[n].__len__()):     #per ogni valore nella lista di risultati di diagnosi (tutti uguali)
                        listareturn[n].append(n)                  #aggiunge il numero corrispondente alla posizione in classificazioni
        else:
            for i in range(0,lista.__len__()):
                l=lista[i].replace(".","")
                for n in range(0,classifica.__len__()):
                    if l==classifica[n]:
                        listareturn.append(n)

        return listareturn

    def convertinumero(self,lista,sottoliste):
        listareturn=[]
        for n in range(0,lista.__len__()):        #lista di attributi
            listareturn.append([])
            if sottoliste:
                for i in range(0,lista[n].__len__()):      #lista di valori per ogni attributo
                    listareturn[n].append([])

        for c in range(0,lista.__len__()):        #ciclo per ogni attributo da convertire
            if sottoliste:
                for n in range(0,lista[0].__len__()):   #ciclo per ogni diagnosi di ogni attributo
                    for i in range(0,lista[c][n].__len__()):   #ciclo per ogni attributo
                        try:
                            float(lista[c][n][i])              #se è un numero lo aggiunge alla lista
                            listareturn[c][n].append(float(lista[c][n][i]))
                        except ValueError:
                            listareturn[c][n].append(-1)                 #gli attributi mancanti vengono sostituiti con -1
            else:
                for i in range(0,lista[c].__len__()):   #ciclo per ogni attributo
                        try:
                            float(lista[c][i])              #se è un numero lo aggiunge alla lista
                            listareturn[c].append(float(lista[c][i]))
                        except ValueError:
                            listareturn[c].append(-1)                 #gli attributi mancanti vengono sostituiti con -1
        return listareturn

    def dividipersesso(self,lista,x):
        listam=[]
        listaf=[]
        for n in range(0,lista.__len__()):        #lista di attributi
            listam.append([])
            listaf.append([])
            for i in range(0,lista[n].__len__()):      #lista di valori per ogni attributo
                listam[n].append([])
                listaf[n].append([])

        #consideriamo sex in posizione x nella lista. Tutti gli altri valori vengono divisi in base al valore (1 o 0) in quella lista
        for c in range(0,lista[x].__len__()):           #c: contatore diagnosi
            for i in range(0,lista[x][c].__len__()):    #i: contatore elementi sottolista
                if lista[x][c][i]==1:
                    for n in range(0,lista.__len__()):  #n: contatore attributi
                        listam[n][c].append(lista[n][c][i])     #aggiunge alla listam solo i valori con sex=1 ovvero maschio
                elif lista[x][c][i]==0:
                    for n in range(0,lista.__len__()):  #n: contatore attributi
                        listaf[n][c].append(lista[n][c][i])     #i valori diversi da 0 e da 1 (ovvero non presenti o errati) non vengono salvati

        return [listam,listaf]
