class Analisi_Attributi:

    def frequenzaboolean(self,lista):
        listareturn=[]
        for n in range(0,lista.__len__()):        #lista di attributi
            listareturn.append([])

        for c in range(0,lista.__len__()):
            for n in range(0,lista[0].__len__()):
                k=0     #numero di occorrenze positive
                t=lista[c][n].__len__()    #numero totale di malattie diagnosticate di cui si abbia l'attributo registrato
                for i in range(0,lista[c][n].__len__()):
                    if lista[c][n][i]==1:  #contiamo il numero di occorrenze positive
                        k=k+1
                    elif lista[c][n][i]==-1:    #se non abbiamo il dato si toglie dal totale l'attributo per il calcolo della frequenza
                        t=t-1
                if t!=0:
                    listareturn[c].append(k/t)
                else:
                    listareturn[c].append(-1)       #se non abbiamo il dato mettiamo come frequenza -1
        return listareturn

    def mediaevarianza(self,lista):
        listamedia=[]
        listavarianza=[]
        for n in range(0,lista.__len__()):        #lista di attributi
            listamedia.append([])
            listavarianza.append([])

        for c in range(0,lista.__len__()):    #ciclo per ogni attributo
            for n in range(0,lista[c].__len__()):   #ciclo per ogni possibile diagnosi
                if(lista[c][n].__len__()!=0):
                    listamedia[c].append(self.media(lista[c][n]))     #aggiunge la media e la varianza calcolata in coda alla lista per ogni diagnosi
                    listavarianza[c].append(self.varianza(lista[c][n]))
                else:
                    listamedia[c].append(-1)     #se la malattia non è mai stata  diagnosticata poniamo -1(0 casi di valori registrati)
                    listavarianza[c].append(-1)
        return([listamedia,listavarianza])

    def fuorirange(self,lista,soglieminori,sogliemaggiori,alfa):
        listafmaggiori=[]
        listafminori=[]
        listainrange=[]
        for n in range(0,lista.__len__()):        #lista di attributi
            listafmaggiori.append([])
            listafminori.append([])
            listainrange.append([])

        for c in range(0,lista.__len__()):
            for n in range(0,lista[c].__len__()):
                k=0     #numero di occorrenze fuori range maggiori
                j=0     #numero di occorrenze fuori range minori
                t=lista[c][n].__len__()    #numero totale di malattie diagnosticate di cui si abbia l'attributo registrato
                for i in range(0,lista[c][n].__len__()):
                    if lista[c][n][i]>sogliemaggiori[c]:  #contiamo il numero di occorrenze fuori range
                        k=k+1
                    elif lista[c][n][i]<soglieminori[c]:
                        j=j+1
                    elif lista[c][n][i]==-1:    #se non abbiamo il dato si toglie dal totale l'attributo per il calcolo della frequenza
                        t=t-1
                if t!=0:
                    #alfa=0 semplice occorrenze/totali, alfa>0, <1: v.a multinomiale
                    listafminori[c].append((j+alfa)/(t+alfa))      #tre valori per ogni malattia,
                    listafmaggiori[c].append((k+alfa)/(t+alfa))    #uno per la frequenza con cui i valori sono sottorange e uno per i sopra
                    listainrange[c].append((1-j-k+alfa)/(t+alfa))  #e uno per quelli dentro il range
                else:
                    listafminori[c].append(-1)       #se non abbiamo il dato mettiamo come frequenza -1
                    listafmaggiori[c].append(-1)
                    listainrange[c].append(-1)

        return [listafminori,listafmaggiori,listainrange]

    def media(self,lista):
        t=0
        n=0
        for i in lista:
            if i>0:
                t=t+i
                n=n+1

        r=-1
        if n!=0:
            r=t/n
        return r

    def varianza(self,lista):
        m=self.media(lista)
        l=[]
        for i in lista:
           if i>0:
               l.append((i-m)**2)

        if l.__len__()==1 and l[0]==0:
            r=0
        else:
            r=self.media(l)
        return r
