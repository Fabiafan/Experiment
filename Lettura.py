import os

class Lettura:

    def recuperadati(self,path,namefile):
        os.chdir(path)
        in_file = open(namefile, "r")
        text = in_file.read()
        in_file.close()
        return text
