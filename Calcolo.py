import numpy as np

class Calcolo:

    def probabilitacondizionata(self,listabooleani,listafrequenze,listanumerici,listamedie,listavarianze,paziente,malattia):
        P=[]    #lista di probabilità condizionata per ogni valore di attributo i del paziente supponendo abbia la malattia Y

        #i valori booleani vengono trattati tramite v.a. Bernuliana
        for i in range(0,listabooleani.__len__()):
            if  listafrequenze[i][malattia]!=-1:      #se non si hanno abbastanza dati in training si mette -1
                if listabooleani[i][paziente]!=-1:    #se per questo paziente manca un dato si mette 1 che non varia la moltiplicazione finale
                    P.append((listabooleani[i][paziente]*listafrequenze[i][malattia]+(1-listabooleani[i][paziente])*(1-listafrequenze[i][malattia])))
                else:
                    P.append(1)
            else:
                P.append(-1)

        #i valori numerici vengono trattati tramite v.a. Gaussiana
        for i in range(0,listanumerici.__len__()):
            if listamedie[i][malattia]!=-1:
                if listanumerici[i][paziente]!=-1 and listavarianze[i][malattia]>0:
                    P.append( (1/np.sqrt(2*np.pi*listavarianze[i][malattia]))*np.exp( -((listanumerici[i][paziente]-listamedie[i][malattia])**2)/(2*listavarianze[i][malattia]) ))
                else:
                    P.append(1)
            else:
                P.append(-1)

        return P

    def probabilitamalattia(self,probabilitacondizionate,indiciprobabilita,pmalattia):
        P=pmalattia   #probabilità che il paziente con questi attributi abbia la  malattia Y

        if pmalattia>0.001:
            for i in range(0,probabilitacondizionate.__len__()):  #moltiplichiamo tutte le probabilità condizionate
                if probabilitacondizionate[i]!=-1 and indiciprobabilita.__contains__(i):
                    P=P*probabilitacondizionate[i]
        else:               #altrimenti il sistema dichiara che non ha abbastanza dati per fare una previsione affidabile
            P=-1

        return P

    def previsione(self,listabool,listafreq,listanume,listamed,listavar,pazien,classificazione,indiciprob,probmalattia):
        listaresult=[]
        #lista delle probabilita di diagnosi previste dal sistema

        for c in range(0,classificazione.__len__()):
            Pcond=self.probabilitacondizionata(listabool,listafreq,listanume,listamed,listavar,pazien,c)
            P=self.probabilitamalattia(Pcond,indiciprob,probmalattia[c])
            if probmalattia[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                listaresult.append(P)
            else:
                listaresult.append(-1)      #probabilita sconosciuta

        return listaresult

    def diagnosi(self,listabool,listafreq,listanume,listamed,listavar,pazien,classificazione,indiciprob,probmalattia):
        listaresult=[]

        listaresult=self.previsione(listabool,listafreq,listanume,listamed,listavar,pazien,classificazione,indiciprob,probmalattia)

        maxp=0
        max=0
        for i in range(0,listaresult.__len__()):
            if listaresult[i]>maxp:
                max=i
                maxp=listaresult[i]

        return max

    def test(self,listabool,listafreq,listanume,listamed,listavar,classificazione,indiciprob,probmalattia,result):
        listadiagnosi=[]        #lista delle diagnosi fatte dal sistema per ogni paziente
        nerrori=0               #numero di diagnosi errate fatte dal sistema
        nerrorip=0              #numero errori di diagnosi positive
        totalep=0               #totale di diagnosi positive (diverse da negative)

        listanoncalcolabili=[]              #lista delle diagnosi che il sistema non può fare per mancanza di dati
        for i in range(0,probmalattia.__len__()):
            if probmalattia[i]==0 or probmalattia[i]==-1:
                listanoncalcolabili.append(i)

        for i in range(0,listabool[0].__len__()):          #ogni valore in una qualunque lista è un paziente: ciclo per ogni paziente
            d=self.diagnosi(listabool,listafreq,listanume,listamed,listavar,i,classificazione,indiciprob,probmalattia)
            listadiagnosi.append(d)
            if result[i]!=0:
                totalep=totalep+1
            if d!=result[i]: #and not listanoncalcolabili.__contains__(result[i]):      #se la diagnosi fatta è sbagliata aggiungiamo +1 agli errori
                nerrori=nerrori+1                  #(tranne nel caso in cui nel training non c'erano abbastanza dati su quella malattia per una previsione affidabile)
                if result[i]!=0:
                    nerrorip=nerrorip+1

        perrorep=nerrorip/totalep
        perrore=nerrori/listabool[0].__len__()             #Perrore= nerrori/totale
        listaresult=[listadiagnosi,nerrori,perrore,perrorep]     #restituisce la lista delle diagnosi, il numero di errori e la percentuale di errore

        return listaresult

    def probabilitacondizionata2(self,listabooleani,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,paziente,malattia):
        P=[]    #lista di probabilità condizionata per ogni valore di attributo i del paziente supponendo abbia la malattia Y

        #i valori booleani vengono trattati tramite v.a. Bernuliana
        for i in range(0,listabooleani.__len__()):
            if listabooleani[i][paziente]!=-1 and listafbool[i][malattia]!=-1:        #escludiamo i dati mancanti dal calcolo della probabilità
                P.append((listabooleani[i][paziente]*listafbool[i][malattia]+(1-listabooleani[i][paziente])*(1-listafbool[i][malattia])))   #paziente 14, malattia 0
            else:
                P.append(-1)

        #i valori numerici vengono trattati diversamente, la probabilità calcolata è la probabilità che quel valore sia minore del range o nel range o maggiore
        for i in range(0,listanumerici.__len__()):
            if listanumerici[i][paziente]!=-1 and listafmaggiori[i][malattia]!=-1 and listafminori[i][malattia]!=-1:         #escludiamo i valori mancanti dal calcolo
                if(listanumerici[i][paziente]<listarangeminori[i]):
                    P.append(listafminori[i][malattia])
                elif (listanumerici[i][paziente]>listarangemaggiori[i]):
                    P.append(listafmaggiori[i][malattia])
                else:
                    P.append(1-listafmaggiori[i][malattia]-listafminori[i][malattia])
            else:
                P.append(-1)

        return P

    def previsione2(self,listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,pazien,classificazione,indiciprob,probmalattia):
        listaresult=[]          #lista delle probabilita di diagnosi previste dal sistema

        for c in range(0,classificazione.__len__()):
            Pcond=self.probabilitacondizionata2(listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,pazien,c)
            P=self.probabilitamalattia(Pcond,indiciprob,probmalattia[c])
            if probmalattia[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                listaresult.append(P)
            else:
                listaresult.append(-1)      #probabilita sconosciuta

        return listaresult

    def diagnosi2(self,listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,pazien,classificazione,indiciprob,probmalattia):
        listaresult=[]

        listaresult=self.previsione2(listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,pazien,classificazione,indiciprob,probmalattia)

        maxp=0
        max=0
        for i in range(0,listaresult.__len__()):
            if listaresult[i]>maxp:
                max=i
                maxp=listaresult[i]

        return max

    def test2(self,listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,classificazione,indiciprob,probmalattia,result):
        listadiagnosi=[]        #lista delle diagnosi fatte dal sistema per ogni paziente
        nerrori=0               #numero di diagnosi errate fatte dal sistema
        nerrorip=0              #numero errori di diagnosi positive
        totalep=0               #totale di diagnosi positive (diverse da negative)

        listanoncalcolabili=[]              #lista delle diagnosi che il sistema non può fare per mancanza di dati
        for i in range(0,probmalattia.__len__()):
            if probmalattia[i]==0 or probmalattia[i]==-1:
                listanoncalcolabili.append(i)

        for i in range(0,listabool[0].__len__()):          #ogni valore in una qualunque lista è un paziente: ciclo per ogni paziente
            d=self.diagnosi2(listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,i,classificazione,indiciprob,probmalattia)
            listadiagnosi.append(d)
            if result[i]!=0:
                totalep=totalep+1
            if d!=result[i]: #and not listanoncalcolabili.__contains__(result[i]):      #se la diagnosi fatta è sbagliata aggiungiamo +1 agli errori
                nerrori=nerrori+1                  #(tranne nel caso in cui nel training non c'erano abbastanza dati su quella malattia per una previsione affidabile)
                if result[i]!=0:
                    nerrorip=nerrorip+1

        perrorep=nerrorip/totalep
        perrore=nerrori/listabool[0].__len__()             #Perrore= nerrori/totale
        listaresult=[listadiagnosi,nerrori,perrore,perrorep]     #restituisce la lista delle diagnosi, il numero di errori e la percentuale di errore

        return listaresult

    def previsionesex(self,listabool,listafreq,listanume,listamed,listavar,pazien,classificazione,indiciprob,probmalattia,listasex):
        listaresult=[]          #lista delle probabilita di diagnosi previste dal sistema

        #le liste vengono suddivise in sottoliste in base al genere. La 0 è qulla maschile, la 1 è quella femminile, la 2 quella totale
        listafreqm=listafreq[0]
        listafreqf=listafreq[1]
        listafreqx=listafreq[2]
        listamedm=listamed[0]
        listamedf=listamed[1]
        listamedx=listamed[2]
        listavarm=listavar[0]
        listavarf=listavar[1]
        listavarx=listavar[2]
        probmalattiam=probmalattia[0]
        probmalattiaf=probmalattia[1]
        probmalattiax=probmalattia[2]

        for c in range(0,classificazione.__len__()):
            #discriminiamo il calcolo in base al sesso del paziente. 1 maschio, 0 femmina
            if listasex[pazien]==1 and listavarm[0][c]!=0:     #controlliamo il sesso del paziente e se ci sono almeno due record per quel sesso e quella malattia in memoria
                Pcond=self.probabilitacondizionata(listabool,listafreqm,listanume,listamedm,listavarm,pazien,c)
                P=self.probabilitamalattia(Pcond,indiciprob,probmalattiam[c])
                if probmalattiam[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                    listaresult.append(P)
                else:
                    listaresult.append(-1)      #probabilita sconosciuta
            elif listasex[pazien]==0 and listavarf[0][c]!=0:
                Pcond=self.probabilitacondizionata(listabool,listafreqf,listanume,listamedf,listavarf,pazien,c)
                P=self.probabilitamalattia(Pcond,indiciprob,probmalattiaf[c])
                if probmalattiaf[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                    listaresult.append(P)
                else:
                    listaresult.append(-1)      #probabilita sconosciuta
            else:       #nel caso in cui il paziente in questione non avesse registrato il sesso in database usiamo media, varianza e Pmalattia generali
                Pcond=self.probabilitacondizionata(listabool,listafreqx,listanume,listamedx,listavarx,pazien,c)
                P=self.probabilitamalattia(Pcond,indiciprob,probmalattiax[c])
                if probmalattiax[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                    listaresult.append(P)
                else:
                    listaresult.append(-1)      #probabilita sconosciuta

        return listaresult

    def diagnosisex(self,listabool,listafreq,listanume,listamed,listavar,pazien,classificazione,indiciprob,probmalattia,listasex):
        listaresult=[]

        listaresult=self.previsionesex(listabool,listafreq,listanume,listamed,listavar,pazien,classificazione,indiciprob,probmalattia,listasex)

        maxp=0
        max=0
        for i in range(0,listaresult.__len__()):
            if listaresult[i]>maxp:
                max=i
                maxp=listaresult[i]

        return max

    def testsex(self,listabool,listafreq,listanume,listamed,listavar,classificazione,indiciprob,probmalattia,result,listasex):
        listadiagnosi=[]        #lista delle diagnosi fatte dal sistema per ogni paziente
        nerrori=0               #numero di diagnosi errate fatte dal sistema
        nerrorip=0              #numero errori di diagnosi positive
        totalep=0               #totale di diagnosi positive (diverse da negative)

        listanoncalcolabili=[]              #lista delle diagnosi che il sistema non può fare per mancanza di dati
        for i in range(0,probmalattia.__len__()):
            if probmalattia[2][i]==0 or probmalattia[2][i]==-1:
                listanoncalcolabili.append(i)

        for i in range(0,listabool[0].__len__()):          #ogni valore in una qualunque lista è un paziente: ciclo per ogni paziente
            d=self.diagnosisex(listabool,listafreq,listanume,listamed,listavar,i,classificazione,indiciprob,probmalattia,listasex)
            listadiagnosi.append(d)
            if result[i]!=0:
                totalep=totalep+1
            if d!=result[i]:    #and not listanoncalcolabili.__contains__(result[i]):      #se la diagnosi fatta è sbagliata aggiungiamo +1 agli errori
                nerrori=nerrori+1                  #(tranne nel caso in cui nel training non c'erano abbastanza dati su quella malattia per una previsione affidabile)
                if result[i]!=0:
                    nerrorip=nerrorip+1

        perrorep=nerrorip/totalep
        perrore=nerrori/listabool[0].__len__()             #Perrore= nerrori/totale
        listaresult=[listadiagnosi,nerrori,perrore,perrorep]     #restituisce la lista delle diagnosi, il numero di errori e la percentuale di errore

        return listaresult

    def previsione2sex(self,listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,pazien,classificazione,indiciprob,probmalattia,listasex,listapreg):
        listaresult=[]          #lista delle probabilita di diagnosi previste dal sistema

        #le liste vengono suddivise in sottoliste in base al genere. La 0 è qulla maschile, la 1 è quella femminile, la 2 quella totale
        #se c'è incinta essa è la due e la 3 è quella totale
        listafboolm=listafbool[0]
        listafboolf=listafbool[1]
        listafboolx=listafbool[2]
        listafminorim=listafminori[0]
        listafminorif=listafminori[1]
        listafminorix=listafminori[2]
        listafmaggiorim=listafmaggiori[0]
        listafmaggiorif=listafmaggiori[1]
        listafmaggiorix=listafmaggiori[2]
        listarangeminorim=listarangeminori[0]
        listarangeminorif=listarangeminori[1]
        listarangeminorip=listarangeminori[2]
        listarangeminorix=listarangeminori[3]
        listarangemaggiorim=listarangemaggiori[0]
        listarangemaggiorif=listarangemaggiori[1]
        listarangemaggiorip=listarangemaggiori[2]
        listarangemaggiorix=listarangemaggiori[3]
        probmalattiam=probmalattia[0]
        probmalattiaf=probmalattia[1]
        probmalattiax=probmalattia[2]

        for c in range(0,classificazione.__len__()):
            #discriminiamo il calcolo in base al sesso del paziente. 1 maschio, 0 femmina
            if listasex[pazien]==1 and probmalattiam[c]>0.001:     #controlliamo il sesso del paziente e se ci sono sufficienti record per quel sesso e quella malattia in memoria
                Pcond=self.probabilitacondizionata2(listabool,listafboolm,listanumerici,listafminorim,listafmaggiorim,listarangeminorim,listarangemaggiorim,pazien,c)
                P=self.probabilitamalattia(Pcond,indiciprob,probmalattiam[c])
                if probmalattiam[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                    listaresult.append(P)
                else:
                    listaresult.append(-1)      #probabilita sconosciuta
            elif listasex[pazien]==0 and probmalattiaf[c]>0.001:
                if listapreg[pazien]==1:                #se la donna è incinta usiamo il range di valori normali delle donne incinta
                    Pcond=self.probabilitacondizionata2(listabool,listafboolf,listanumerici,listafminorif,listafmaggiorif,listarangeminorim,listarangemaggiorim,pazien,c)
                    P=self.probabilitamalattia(Pcond,indiciprob,probmalattiaf[c])
                else:
                    Pcond=self.probabilitacondizionata2(listabool,listafboolf,listanumerici,listafminorif,listafmaggiorif,listarangeminorim,listarangemaggiorim,pazien,c)
                    P=self.probabilitamalattia(Pcond,indiciprob,probmalattiaf[c])
                if probmalattiaf[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                    listaresult.append(P)
                else:
                    listaresult.append(-1)      #probabilita sconosciuta
            else:       #nel caso in cui il paziente in questione non avesse registrato il sesso in database usiamo frequenze e range dei valori totali (senza distinzionie di sesso)
                Pcond=self.probabilitacondizionata2(listabool,listafboolx,listanumerici,listafminorix,listafmaggiorix,listarangeminorix,listarangemaggiorix,pazien,c)
                P=self.probabilitamalattia(Pcond,indiciprob,probmalattiax[c])
                if probmalattiax[c]!=-1:             #se probmalattia=-1 allora il sistema non riesce ad fare una previsione affidabile per quella malattia
                    listaresult.append(P)
                else:
                    listaresult.append(-1)      #probabilita sconosciuta

        return listaresult

    def diagnosi2sex(self,listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,pazien,classificazione,indiciprob,probmalattia,listasex,listapreg):
        listaresult=[]

        listaresult=self.previsione2sex(listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,pazien,classificazione,indiciprob,probmalattia,listasex,listapreg)

        maxp=0
        max=0
        for i in range(0,listaresult.__len__()):
            if listaresult[i]>maxp:
                max=i
                maxp=listaresult[i]

        return max

    def test2sex(self,listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,classificazione,indiciprob,probmalattia,result,listasex,listapreg):
        listadiagnosi=[]        #lista delle diagnosi fatte dal sistema per ogni paziente
        nerrori=0               #numero di diagnosi errate fatte dal sistema
        nerrorip=0              #numero errori di diagnosi positive
        totalep=0               #totale di diagnosi positive (diverse da negative)

        listanoncalcolabili=[]              #lista delle diagnosi che il sistema non può fare per mancanza di dati
        for i in range(0,probmalattia[2].__len__()):
            if probmalattia[2][i]==0 or probmalattia[2][i]==-1:
                listanoncalcolabili.append(i)

        for i in range(0,listabool[0].__len__()):          #ogni valore in una qualunque lista è un paziente: ciclo per ogni paziente
            d=self.diagnosi2sex(listabool,listafbool,listanumerici,listafminori,listafmaggiori,listarangeminori,listarangemaggiori,i,classificazione,indiciprob,probmalattia,listasex,listapreg)
            listadiagnosi.append(d)
            if result[i]!=0:
                totalep=totalep+1
            if d!=result[i]: #and not listanoncalcolabili.__contains__(result[i]):      #se la diagnosi fatta è sbagliata aggiungiamo +1 agli errori
                nerrori=nerrori+1                  #(tranne nel caso in cui nel training non c'erano abbastanza dati su quella malattia per una previsione affidabile)
                if result[i]!=0:
                    nerrorip=nerrorip+1

        perrorep=nerrorip/totalep
        perrore=nerrori/listabool[0].__len__()             #Perrore= nerrori/totale
        listaresult=[listadiagnosi,nerrori,perrore,perrorep]     #restituisce la lista delle diagnosi, il numero di errori e la percentuale di errore

        return listaresult
