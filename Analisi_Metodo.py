class Analisi_Metodo:

    def analisirisultatotest(self,listadiagnosi,result,classificazione):
        listaerrori=[]       #numero di errori per ogni possibile diagnosi
        listaerrorip=[]      #percentuale di errori per ogni possibile diagnosi
        listatotali=[]       #totali result divisi per diagnosi

        for i in range(0,classificazione.__len__()):
            listaerrori.append(0)
            listaerrorip.append(0)
            listatotali.append(0)

        for i in range(0,listadiagnosi.__len__()):              #ad ogni posizione corrispondente alla diagnosi in classificazione data da result
            listatotali[result[i]]=listatotali[result[i]]+1     #al totale si aggiunge uno per ogni diagnosi nella posizione data da result
            if listadiagnosi[i]!=result[i]:                     #se sono diversi c'è un errore che viene aggiunto al totale conteggiato
                listaerrori[result[i]]=listaerrori[result[i]]+1

        for i in range(0,listatotali.__len__()):           #per ogni elemento della lista si calcola la percentuale di errore
            if(listatotali[i]!=0):
                listaerrorip[i]=listaerrori[i]/listatotali[i]
            else:
                listaerrorip[i]=0

        return [listaerrori,listatotali,listaerrorip]


    def analisirisultatotest2(self,listadiagnosi,result,classificazione):
        listaerrori=[]       #numero di errori per ogni possibile diagnosi
        listaerrorip=[]      #percentuale di errori per ogni possibile diagnosi
        listatotali=[]       #totali result divisi per diagnosi

        for i in range(0,classificazione.__len__()):
            listaerrori.append(0)
            listaerrorip.append(0)
            listatotali.append(0)

        for i in range(0,listadiagnosi.__len__()):              #ad ogni posizione corrispondente alla diagnosi in classificazione data da result
            listatotali[result[i]]=listatotali[result[i]]+1     #al totale si aggiunge uno per ogni diagnosi nella posizione data da result
            if listadiagnosi[i]!=result[i]:                     #se sono diversi c'è un errore che viene aggiunto al totale conteggiato
                listaerrori[result[i]]=listaerrori[result[i]]+1

        for i in range(0,listatotali.__len__()):           #per ogni elemento della lista si calcola la percentuale di errore
            if(listatotali[i]!=0):
                listaerrorip[i]=listaerrori[i]/listatotali[i]
            else:
                listaerrorip[i]=0

        return [listaerrori,listatotali,listaerrorip]
