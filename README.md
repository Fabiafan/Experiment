# Esperimento di classificazione delle malattie della tiroide

Questa cartella contiene i file che implementano l'esperimento desctritto nella tesi di laurea in Ingegneria dell'Informazione di Fabiano Fantini A.A. 2017/2018, intitolata: "La diagnosi delle malattie attraverso il machine learning: un esperimento con classificatore bayesiano".

L'esperimento consiste in un confronto tra un algoritmo bayesiano naif classico ed uno modificato, creato appositamente in questa tesi.

Il contenuto della cartella è il seguente:

  - Dataset di training e di testing in formato .csv
  - Classi utilizzate nell'esperimento in linguaggio Python
  - Main dell'esperimento in linguaggio Python chiamato "esperimento"
  - Risultato dell'esperimento in formato .csv

License
----

Fabiano Fantini
**Bianovivacity@gmail.com**