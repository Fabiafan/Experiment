import os

class Scrittura:

    def scrivirisultati(self,path,namefile,writelist1,writelist2):
        os.chdir(path)
        out_file = open(namefile, "w")
        fatto=0
        if writelist1.__len__()==writelist2.__len__():
            out_file.write("risultati reali"+";"+"risultati calcolati"+"\n")
            for i in range(0,writelist1.__len__()):
                s=str(writelist1[i])+";"+str(writelist2[i])+"\n"
                out_file.write(s)
            fatto=1
        out_file.close()
        return fatto
